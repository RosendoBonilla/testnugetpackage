﻿using OpenCvSharp;
using OpenCvSharp.Extensions;
using System.Drawing;


namespace NugetExample
{
    public class OcvBasicFunctions
    {
        public Bitmap GetBitmapImage(string imagePath)
        {
            return Cv2.ImRead(imagePath, ImreadModes.AnyDepth).ToBitmap();
        }
    }
}
